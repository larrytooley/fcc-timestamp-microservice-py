# Free Code Camp Timestamp Microservice in Python

Inspired by [this][1] Free Code Camp challenge. You can find the Node.js version of my solution [here][2]

App running on Heroku [here][3].

[1]: https://www.freecodecamp.com/challenges/timestamp-microservice
[2]: https://github.com/larrytooley/fcc-timestamp-microservice
[3]: https://protected-escarpment-26541.herokuapp.com/