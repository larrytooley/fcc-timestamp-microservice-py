from flask import Flask
from flask import request
from flask import render_template
from flask import url_for
import os
import json
from datetime import datetime

app = Flask(__name__)
port = int(os.environ.get('PORT', 5000))


# Helper Functions
def isnumber(param):
    """
        Helper function to determine if query parameter is a number.
    """
    try:
        float(param)
        return True
    except ValueError:
        return False


@app.route("/")
def root():
    """
    Renders a template with api instructions
    """

    return render_template('index.html')


@app.route("/<timestamp>")
def api(timestamp=None):
    """
    Returns Unix and natural language dates if a valid timestamp is entered.
    Returns null if there is no valid input.
    """

    null = None
    unix = null
    natural = null

    if isnumber(timestamp):
        try:
            date = datetime.fromtimestamp(int(timestamp))
            unix = int(date.timestamp())
            natural = date.strftime("%B %d, %Y")

        except ValueError:
            pass

    else:
        try:
            date = datetime.strptime(timestamp, '%B %d, %Y')
            unix = int(date.timestamp())
            natural = timestamp

        except ValueError:
            pass


    data = {
        "Unix": unix,
        "natural": natural
        }

    json_data = json.JSONEncoder().encode(data)

    return json_data

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=port)
